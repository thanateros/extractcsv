package main

import (
	"bufio"
	"encoding/csv"
	"flag"
	"fmt"
	"io"
	"log"
	"os"
	"path/filepath"
	"regexp"
	"strings"
)

func main() {

	rtfRegex := regexp.MustCompile(`{\*?\\.+(;})|\s?\\[A-Za-z0-9]+|\s?{\s?\\[A-Za-z0-9]+\s?|\s?}\s?`)
	slashesRegex := regexp.MustCompile(`\\`)
	lineNo := 0
	extractedCSV := [][]string{}

	inputCSVFilePtr := flag.String("file", "", "path to input CSV file that needs text extracted from RTF-encoded field.\nExample: 'extractCSV -file path/to/csv/file'.")

	flag.Parse()

	outputCSV := strings.TrimSuffix(*inputCSVFilePtr, filepath.Ext(*inputCSVFilePtr)) + "_extracted.csv"

	csvFile, _ := os.Open(*inputCSVFilePtr)
	defer csvFile.Close()

	reader := csv.NewReader(bufio.NewReader(csvFile))

	outputRow := []string{}

	for {
		line, err := reader.Read()

		if err == io.EOF {
			break
		} else if err != nil {
			fmt.Printf("\nThere was an error: %v\n", err)
			fmt.Println("Aborting. \nType 'extractCSV -h' for assistance.")
			os.Exit(1)
		}

		for i := 0; i < len(line); i++ {

			if rtfRegex.FindStringIndex(line[i]) == nil {
				outputRow = append(outputRow, line[i])
				continue
			}

			tmp := strings.TrimSpace(slashesRegex.ReplaceAllString(rtfRegex.ReplaceAllString(line[i], ""), ""))

			if tmp == "" {
				outputRow = append(outputRow, tmp)
				continue
			}

			outputRow = append(outputRow, tmp)
		}

		extractedCSV = append(extractedCSV, outputRow)
		outputRow = []string{}

		lineNo++
	}

	fmt.Println("\n Finished removing RTF tags.\nPreparing to write new file.")

	outputFile, err := os.Create(outputCSV)
	checkError("Cannot create file: ", err)
	defer outputFile.Close()

	writer := csv.NewWriter(outputFile)
	defer writer.Flush()

	for _, value := range extractedCSV {
		err := writer.Write(value)
		checkError("Cannot write to file: ", err)
	}

	fmt.Printf("==========\nCompleted removing RTF cruft.\nNew file generated: %v\n==========\n", outputCSV)
}

func checkError(message string, err error) {
	if err != nil {
		log.Fatal(message, err)
	}
}
