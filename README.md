# extractCSV

A small command-line utility to consume a CSV file exported from a source 
(like a database) where any of the fields have unwanted RTF markup and one 
wishes to only keep the text in those fields creating a new CSV with all 
the RTF junk removed.

## TL;DR

```bash
$ ls
sample.csv
$ extractCSV -file sample.csv

Finished removing RTF tags.
Preparing to write new file.
==========
Completed removing RTF cruft.
New file generated: sample_extracted.csv
==========

$ls
sample.csv sample_extracted.csv
```

## Get the software

```go
$ go get gitlab.com/thanateros/extractcsv
```

## Install the software

```bash
$ cd $GOPATH/src/gitlab.com/thanateros/extractCSV
$ go install
```

## Use the software

The command takes one flag: `-file path/to/csv/file`.

It strips all RTF markup from each field and writes a new CSV to the same
source folder with a modified name -- it has `_extracted` after the original
file name, e.g., `sample.csv` input file generates an output file called
`sample_extracted.csv`. It does not alter the source CSV file.

## Inspiration 

I needed to migrate data from an old RDBMS to a new RDBMS via CSV export and
import. The new DB system had an import wizard that could handle everything but 
extracting the text from RTF fields (who in their right mind would ever use RTF?
) -- like I said, it was a *very* legacy DB.

**Go** and good 'ol **regular expressions** to the rescue.

I found this regexp:

```
{\*?\\.+(;})|\s?\\[A-Za-z0-9]+|\s?{\s?\\[A-Za-z0-9]+\s?|\s?}\s?
```

on this page: https://stackoverflow.com/questions/188545/regular-expression-for-extracting-text-from-an-rtf-string just over halfway down the page.

It matches the RTF junk, so regex search and replace took care of that but I was left with a bunch of slashes around my text.

One more run through regex search and replace to remove the leading and trailing left-slashes, a little `strings.Trim` for left-over leading and trailing whitespace and voila!

Then write it all back to a differenct CSV.  Ready for clean import into the new database.

Enjoy.  Hope someone finds this tool usefull. Improvement suggestions, PRs, whatnot ... all welcome.  There is room for improvement -- I slapped this together quick to get the job done.